const express = require('express')
// const cookieParser = require('cookie-parser')

const app = express()
const consign = require('consign')
const bodyParser = require('body-parser')
const expressLayouts = require('express-ejs-layouts')
const session = require('express-session')
const mongodb = require('mongodb')
require('dotenv').config()

const mongoose = require('mongoose') // orm mongodb

// app.use(cookieParser())

app.use(session({
	name: 'server-session-cookie-id-fixit',
	secret: process.env.HASHJWT,
	saveUninitialized: true,
	resave: true,
	cookie: {
		// secure: true, // se true precisa de https
		maxAge: 2160000000,
		httpOnly: true
	}
}))

mongoose.connect(`mongodb+srv://${process.env.USERDB}:${process.env.PASSDB}@cluster0-apglo.mongodb.net/${process.env.NAMEDB}?retryWrites=true`, {
	useCreateIndex: true,
	useNewUrlParser: true
}).catch(err => {
	console.error(err)
})

ObjectId = mongodb.ObjectID
const client = new mongodb.MongoClient(
	'mongodb+srv://' + process.env.USERDB
	+ ':' + process.env.PASSDB + '@cluster0-apglo.mongodb.net/'
	+ process.env.NAMEDB + '?retryWrites=true',
	{ useNewUrlParser: true }
)

client.connect(err => {
	if (err) throw err
	db = client.db(process.env.NAMEDB)

	app.listen(process.env.PORT, () => {
		console.info(`RUN IN PORT ${process.env.PORT} (${new Date().toISOString()})`)
	})
})

app.use(bodyParser.urlencoded({ extended: true }))
app.set('view engine', 'ejs')
app.set('views', './src/view')
app.use(express.static('./src/assets'))
app.use(expressLayouts)
consign({ verbose: false }).include('./src/controller').into(app)


const tests = async () => {
	const testsModels = require('./src/models/test.js')
	// await testsModels.testForeignKeyCreate()
	// await testsModels.testForeignKeyGetAndFilter()
}

tests()