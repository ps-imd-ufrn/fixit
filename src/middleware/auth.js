const jwt = require('jsonwebtoken')


const saveTokenJWT = (req, res, user) => {
    const token = jwt.sign({ id: user.id }, process.env.HASHJWT, {
        expiresIn: 86400 // valido por 1 dia
    })

    req.session.tokenjwt = `Bearer ${token}`
    req.session.user = user
}

const logout = (req, res) => {
    req.session.tokenjwt = undefined
    req.session.user = undefined
}

const caseNotLogged = (req, res, isRedirectPageLogin) => {
    if (isRedirectPageLogin) {
        res.redirect('/login')
    }
    return false
}

const verify = async (req, res, args = {}) => {
    const { isRedirectPageLogin, isValidAll, isValidCaseComum, isValidCaseTecnico, isValidCaseGestor } = Object.assign({
        isRedirectPageLogin: true,
        isValidAll: false,
        isValidCaseComum: false,
        isValidCaseTecnico: false,
        isValidCaseGestor: false,
    }, args)

    // const authHeader = req.headers.authorization // `Bearer ${token}// Bearer ${token}`
    const authHeader = req.session.tokenjwt // `Bearer ${token}// Bearer ${token}`
    // sem token
    if (!authHeader) {
        return caseNotLogged(req, res, isRedirectPageLogin)
    }

    const parts = authHeader.split(' ')
    // erro no formato
    if (!parts.lenght === 2) {
        return caseNotLogged(req, res, isRedirectPageLogin)
    }

    const [scheme, token] = parts
    // token mal formatado
    if (!/^Bearer$/i.test(scheme)) {
        return caseNotLogged(req, res, isRedirectPageLogin)
    }

    try {
        const decode = await jwt.verify(token, process.env.HASHJWT)
        // return decode.id // userId
        const user = req.session.user
        if (isValidAll
            || (user.type === 'Comum' && isValidCaseComum)
            || (user.type === 'Tecnico' && isValidCaseTecnico)
            || (user.type === 'Gestor' && isValidCaseGestor)) {
            return user
        } else {
            return caseNotLogged(req, res, isRedirectPageLogin)
        }
        // return req.session.user
    } catch (error) {
        return caseNotLogged(req, res, isRedirectPageLogin)
    }
}


const verifyAll = async (req, res, args = {}) => {
    args = Object.assign({
        isValidAll: true,
    }, args)

    return await verify(req, res, args)
}


const verifyComum = async (req, res, args = {}) => {
    args = Object.assign({
        isValidCaseComum: true,
    }, args)

    return await verify(req, res, args)
}

const verifyTecnico = async (req, res, args = {}) => {
    args = Object.assign({
        isValidCaseTecnico: true,
    }, args)

    return await verify(req, res, args)
}

const verifyGestor = async (req, res, args = {}) => {
    args = Object.assign({
        isValidCaseGestor: true,
    }, args)

    return await verify(req, res, args)
}

module.exports = {
    verify,
    verifyAll,
    verifyComum,
    verifyTecnico,
    verifyGestor,
    saveTokenJWT,
    logout
}