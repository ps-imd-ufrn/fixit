const auth = require('../middleware/auth.js')
const bodyParser = require('body-parser')

module.exports = app => {

	app.use(
	  bodyParser.urlencoded({
	    extended: true
	  })
	)

	app.use(bodyParser.json())
	
	const tb = 'occurrencealerts'

	app.route('/relatorio').get(async (req, res) => {
		if (!await auth.verifyAll(req, res)) return

		db.collection(tb).find().toArray((err, results) => {
			if (err) throw err;
			res.render('relatorios.ejs', { data: results })
		})
	})

	app.route('/relatorio/find').post(async (req, res) => {
		
      
		

		if (!await auth.verifyAll(req, res)) return

		if (req.body.gravidade != null && req.body.tipo != null) {
			var query = { gravidade: req.body.gravidade , tipo: req.body.tipo};
		}else if(req.body.gravidade != null){
			var query = { gravidade: req.body.gravidade };
		}else if (req.body.tipo != null) {
			var query = {tipo: req.body.tipo};
		}

				
		db.collection(tb).find(query).toArray((err, results) => {
			if (err) throw err;
			res.render('relatorios.ejs', { data: results })
		})
	})
}