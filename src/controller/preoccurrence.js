const auth = require('../middleware/auth.js')
const { User } = require('../models/auth')
const { OccurrenceAlert } = require('../models/occurrenceAlert')

module.exports = app => {

	//Listar todos as ocorrências
	app.route('/preoccurrence').get(async (req, res) => {
				
		var userAuth = await auth.verifyAll(req, res);

		if (!userAuth) return

		if(userAuth.type === 'Comum'){
		 	OccurrenceAlert.find({ userAbriuChamado : userAuth }, (err, results) => {
		 		if (err) throw err;	
		 		res.render('preoccurrence-home.ejs', { userAuth : userAuth , data: results })
			});
		} 
		else {
			OccurrenceAlert.find({}, (err, results) => {
				if (err) throw err;	
				res.render('preoccurrence-home.ejs', { userAuth : userAuth , data: results })
			});
		}
	})

	//Novo registro de ocorrência
	app.route('/preoccurrence/new').get(async (req, res) => {
		if (!(await auth.verifyAll(req, res))) return

		res.render('preoccurrence-new.ejs', { data: { _id: 0 } })
	})

	//Editar registro de ocorrência
	app.route('/preoccurrence/new/:id').get(async (req, res) => {
		if (!await auth.verifyAll(req, res)) return

		OccurrenceAlert.findOne({ _id: ObjectId(req.params.id) }, (err, result) => {
			if (err) throw err;

			res.render('preoccurrence-new.ejs', { data: result })
		})
	})

	//Salvar ou Editar ocorrência
	app.route('/preoccurrence/new/:id').post(async (req, res) => {

		var userAuth = await auth.verifyAll(req, res);

		if (!userAuth) return

		const { local, description } = req.body

		if (req.params.id != 0) {
			OccurrenceAlert.findOne({ _id: ObjectId(req.params.id) }, (err, result) => {
				if (err) throw err;

				result.local = local;
				result.description = description;

				result.save((err) => {
					if (err) throw err;
					res.redirect('/preoccurrence')
				})
			})
		} else {
			// Criando uma nova ocorrencia
			var newOccurrenceAlert = OccurrenceAlert({
				userAbriuChamado: userAuth,
				local: local,
				description: description,
			})
			await newOccurrenceAlert.save((err) => {
				if (err) throw err;
				res.redirect('/preoccurrence')
			})
		}

	})

	app.route('/preoccurrence/delete/:id').get(async (req, res) => {
		if (!await auth.verifyAll(req, res)) return

		var occurrence = await OccurrenceAlert.findOne({_id: ObjectId(req.params.id)});

		if(occurrence.situation === 'Aberta'){
			OccurrenceAlert.deleteOne({ _id: ObjectId(req.params.id) }, (err, result) => {
				if (err) throw err;
				res.redirect('/preoccurrence')
			})
		}else{
			return
		}

		
	})

	app.route('/preoccurrence/evaluate/:id').get(async (req, res) => {
		const user = await auth.verify(req, res, {
			isValidCaseTecnico: true,
			isValidCaseGestor: true,
		})
		if (!user) return

		OccurrenceAlert.findOne({ _id: ObjectId(req.params.id) }, (err, result) => {
			if (err) throw err;

			res.render('preoccurrence-evaluate.ejs', { data: result, usuario: user });
		});
	});

	//Salvar atendimento da ocorrência
	app.route('/preoccurrence/evaluate/:id').post(async (req, res) => {

		var userIdAuth = await auth.verifyAll(req, res);

		if (!userIdAuth) return

		const { type, feedback } = req.body

		if (req.params.id != 0) {
			OccurrenceAlert.findOne({ _id: ObjectId(req.params.id) }, (err, result) => {
				if (err) throw err;

				result.type = type;
				result.feedback = feedback;
				result.situation = 'Concluido';
				result.userTecnicoResponsavel = userIdAuth;

				result.save((err) => {
					if (err) throw err;
					res.redirect('/preoccurrence')
				})
			})
		}
	});

	//Atender
	app.route('/preoccurence/setstatus/emandamento/:id').get(async (req, res) => {

		var userIdAuth = await auth.verifyAll(req, res, {
			isValidCaseGestor: true,
			isValidCaseTecnico: true 
		});

		if (!userIdAuth) return

		let o = await OccurrenceAlert.findOne({ _id: ObjectId(req.params.id) })
		o.situation = 'Em andamento'
		await o.save()
		res.redirect(`/preoccurrence/evaluate/${req.params.id}`)
	});

}
