const auth = require('../middleware/auth.js')
const { User } = require('../models/auth.js')

module.exports = app => {

	//Listar todos os usuários
	app.route('/technician').get(async (req, res) => {
        if (!await auth.verifyGestor(req, res)) return

		User.find({}, (err, results) => {
			if (err) throw err;
			results.forEach((result) => {
                if(result.type != 'Comum' ){
                    console.log('Nome:' + result.nome +' CPF:' + result.cpf 
                                +' Tipo:' + result.type);
                }				
			})
			res.render('technician-home.ejs', { data: results })
		});
	})

	//Novo registro de usuário
	app.route('/technician/new').get(async (req, res) => {
		if (!(await auth.verifyGestor(req, res))) return

		res.render('technician-new.ejs', { data: { _id: 0 } })
	})

	//Editar registro de usuário
	app.route('/technician/new/:id').get(async (req, res) => {
		if (!await auth.verifyGestor(req, res)) return

		User.findOne({ _id: ObjectId(req.params.id) }, (err, result) => {
			if (err) throw err;

			res.render('technician-new.ejs', { data: result })
		})
	})

	//Salvar ou Editar usuário
	app.route('/technician/new/:id').post(async (req, res) => {

		var userAuth = auth.verifyGestor(req, res);
		if (!await userAuth) return

        const { nome, email, senha, cpf, type } = req.body

        if (req.params.id == 0) {
            if (await User.findOne({ email }))
            return res.render('technician-new.ejs', {
                alerts: {
                    listErro: [
                        `O e-mail ${email} já está cadastrado`
                    ]
                }
            })

		if (req.params.id != 0) {
			User.findOne({ _id: ObjectId(req.params.id) }, (err, result) => {
				if (err) throw err;

				result.nome = nome;
                result.cpf = cpf;
                result.senha = senha;
                result.type = type;

				result.save((err) => {
					if (err) throw err;
					res.redirect('/technician')
				})
			})
		} else {
            
            

            var newUser = User({
                nome: nome,
                email: email,
                senha: senha,
                cpf: cpf,
                type: type,
            })

			await newUser.save((err) => {
				if (err) throw err;
				res.redirect('/technician')
			})
            }
        }
    })

	app.route('/technician/delete/:id').get(async (req, res) => {
		if (!await auth.verifyGestor(req, res)) return

		User.deleteOne({ _id: ObjectId(req.params.id) }, (err, result) => {
			if (err) throw err;
			res.redirect('/technician')
		})
	})
}
