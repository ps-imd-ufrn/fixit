const bcrypt = require('bcryptjs')

const auth = require('../middleware/auth.js')
const { User } = require('../models/auth.js')

module.exports = app => {
	app.route('/').get(async (req, res) => {
		if (await auth.verifyAll(req, res)) {
			return res.redirect('/preoccurrence')
		}
	})

	app.route('/register').get(async (req, res) => {
		if (await auth.verifyAll(req, res, { isRedirectPageLogin: false })) {
			return res.redirect('/')
		}

		return res.render('register.ejs', {
			alerts: {
				listErro: [
				]
			}
		})
	})

	app.route('/register').post(async (req, res) => {
		if (await auth.verifyAll(req, res, { isRedirectPageLogin: false })) {
			return res.redirect('/')
		}

		const { nome, email, senha, cpf } = req.body
		if (await User.findOne({ email }))
			return res.render('register.ejs', {
				alerts: {
					listErro: [
						`O e-mail ${email} já está cadastrado`
					]
				}
			})

		const user = await User.create({ nome, email, senha, cpf })

		auth.saveTokenJWT(req, res, user.id)
		return res.redirect('/')
	})

	app.route('/login').get(async (req, res) => {
		if (await auth.verifyAll(req, res, { isRedirectPageLogin: false })) {
			return res.redirect('/')
		}

		return res.render('login.ejs', {
		})
	})

	app.route('/login').post(async (req, res) => {
		if (await auth.verifyAll(req, res, { isRedirectPageLogin: false })) {
			return res.redirect('/')
		}
		const { email, senha } = req.body

		let user = await User.findOne({ email }).select('+senha')

		if (!user || (!await bcrypt.compare(senha, user.senha)))
			return res.render('login.ejs', {
				error: 'Usuário ou senha incorreto'
			})

		auth.saveTokenJWT(req, res, user)
		return res.redirect('/')
	})

	app.route('/logout').get(async (req, res) => {
		auth.logout(req, res)
		return res.redirect('/')
	})

}