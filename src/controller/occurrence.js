const auth = require('../middleware/auth.js')
const bodyParser = require('body-parser')



module.exports = app => {

	app.use(
	  bodyParser.urlencoded({
	    extended: true
	  })
	)

	app.use(bodyParser.json())
	
	const tb = 'occurrencealerts'

	app.route('/occurrence').get(async (req, res) => {
		if (!await auth.verifyAll(req, res)) return

		db.collection(tb).find().toArray((err, results) => {
			if (err) throw err;
			res.render('occurrence-home.ejs', { data: results })
		})
	})
	app.route('/occurrencealerts/find').post(async (req, res) => {
		
      
		

		if (!await auth.verifyAll(req, res)) return

		if (req.body.gravidade != null && req.body.tipo != null) {
			var query = { gravidade: req.body.gravidade , tipo: req.body.tipo};
		}else if(req.body.gravidade != null){
			var query = { gravidade: req.body.gravidade };
		}else if (req.body.tipo != null) {
			var query = {tipo: req.body.tipo};
		}

				
		db.collection(tb).find(query).toArray((err, results) => {
			if (err) throw err;
			res.render('preoccurrence-home.ejs', { data: results })
		})
	})

	app.route('/occurrence/new').get(async (req, res) => {
		if (!(await auth.verifyAll(req, res))) return
		res.render('occurrence-new.ejs', { data: { _id: 0 } })
	})

	app.route('/occurrence/new/:id').get(async (req, res) => {
		if (!await auth.verifyAll(req, res)) return
		db.collection(tb).findOne({ _id: ObjectId(req.params.id) }, (err, result) => {
			if (err) throw err;
			res.render('occurrence-new.ejs', { data: result })
		})
	})

	app.route('/occurrence/new/:id').post(async (req, res) => {
		if (!await auth.verifyAll(req, res)) return
		if (req.params.id != 0) {
			db.collection(tb).updateOne({ _id: ObjectId(req.params.id) }, {
				$set: req.body
			}, (err, result) => {
				if (err) throw err;
				res.redirect('/occurrence')
			})
		} else {
			db.collection(tb).insertOne(req.body, (err, result) => {
				if (err) throw err;
				res.redirect('/occurrence')
			})
		}
	})

	app.route('/occurrence/delete/:id').get(async (req, res) => {
		if (!await auth.verifyAll(req, res)) return

		db.collection(tb).deleteOne({ _id: ObjectId(req.params.id) }, (err, result) => {
			if (err) throw err;
			res.redirect('/occurrence')
		})
	})
}