const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')

const User = new mongoose.Schema(
    {
        nome: { type: String, required: true, },
        cpf: { type: String, required: true, },
        email: { type: String, required: true, index: { unique: true }, },
        senha: { type: String, required: true, select: false },
        type: {
            type: String,
            enum: ['Comum', 'Tecnico', 'Gestor'],
            default: 'Comum',
        },
    },
    {
        // cria createdAt e updatedAp
        timestamps: true
    }
)

User.pre('save', async function (next) {
    const hash = await bcrypt.hash(this.senha, 10)
    this.senha = hash
    next()
})

module.exports = {
    User: mongoose.model('User', User),
}