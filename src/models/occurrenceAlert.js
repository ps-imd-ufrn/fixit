const mongoose = require('mongoose')


const OccurrenceAlert = new mongoose.Schema(
    {
        userAbriuChamado: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true, },
        userTecnicoResponsavel: { type: mongoose.Schema.Types.ObjectId, ref: 'User', default: null, },
        local: { type: String, required: true },
        description: { type: String, required: true },
        tipo: {
            type: String,
            enum: [
                null,
                'Lâmpada queimada',
                'Falta de água',
                'Ar-condicionado quebrado',
                'Computador quebrado',
            ],
            default: null,
        },
        situation: {
            type: String,
            enum: ['Aberta', 'Em andamento', 'Concluido', 'Cancelado'],
            default: 'Aberta',
        },
        imgSrc: { type: String, default: null, },
        feedback: { type: String, default: null, },
        gravidade: {
            type: String,
            enum: [null, 'Baixa', 'Media', 'Alta'],
            default: null,
        },
    },
    {
        // cria createdAt e updatedAp
        timestamps: true
    }
)

// OccurrenceAlert.pre('save', async function (next) {
//     next()
// })

module.exports = {
    OccurrenceAlert: mongoose.model('OccurrenceAlert', OccurrenceAlert),
}