const mongoose = require('mongoose')
const auth = require('./auth.js')
const occurrenceAlert = require('./occurrenceAlert.js')

const testForeignKeyCreate = async () => {
    const user = await auth.User.create({
        nome: 'Addan Test',
        email: 'addanfelipe@gmail.com',
        senha: '123456',
        cpf: '097.864.865-45'
    })

    await occurrenceAlert.OccurrenceAlert.create({
        userAbriuChamado: user,
        userTecnicoResponsavel: user,
        local: 'B301',
        description: 'PC não loga no Ubuntu',
        situation: 'Aberta',
    })

    await occurrenceAlert.OccurrenceAlert.create({
        userAbriuChamado: user,
        userTecnicoResponsavel: user,
        local: 'A201',
        description: 'Maquina sem memoria',
        situation: 'Em andamento',
        img: null,
        feedback: 'Vamos comprar mais um pente de RAM',
        gravidade: 'Baixa',
        tipo: 'Lâmpada queimada',
    })
}

const testForeignKeyGetAndFilter = async () => {
    const user = await auth.User.findOne({ email: 'addanfelipe@gmail.com' })
    const occurrenceAlertList = await occurrenceAlert.OccurrenceAlert.find({ userAbriuChamado: user })
    console.log(occurrenceAlertList)
}

module.exports = {
    testForeignKeyCreate, testForeignKeyGetAndFilter
}