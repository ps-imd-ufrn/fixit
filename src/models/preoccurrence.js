const mongoose = require('mongoose')

const PreOccurrence = new mongoose.Schema(
    {
        userId: { type: String, required: true },
        local: { type: String, required: true },
        description: { type: String, required: true },
        situation: { type: String, required: true },
        img: { data: Buffer, contentType: String },
        type: { type: String },
        feedback: { type: String },

    },
    {
        // cria createdAt e updatedAp
        timestamps: true
    }
)

// PreOccurrence.pre('save', async function (next) {
//     next()
// })

module.exports = {
    PreOccurrence: mongoose.model('PreOccurrence', PreOccurrence),
}